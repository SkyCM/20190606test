import java.util.Random;
import java.util.Scanner;

public class No3 {
    public static void main(String[] args){
        Random random = new Random();
        int[] test = new int[10];
        int how = random.nextInt(10)+1;
        int max = 0 , min = 0 , howmin = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.println("請輸入"+how+"個整數(每輸入一個數值後請按下確定)");

        for( int i = 0 ; i < how ; i++ ){
            test[i] = scanner.nextInt();
            if( i == 0 ) { max = test[i]; min = test[i]; }
            if( max < test[i] ){ max = test[i];}
            if( min > test[i] ){ min = test[i];}
        }
        for( int i = 0 ; i < how ; i++ ){
            if( min == test[i] ) howmin++;
        }
        System.out.println("最大值:"+max+" 最小值:"+min+" 重複最小值:"+howmin);
    }
}
